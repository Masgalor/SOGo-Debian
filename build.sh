#!/usr/bin/env bash

set -e

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )" 

# Explain to the user what is going to happen and ask for permission
clear
echo "In order to build a new package it is necessary to install the debian packaging tools."
echo "It is as well necessary to add the source repository of Debian Bullseye to your system."
echo "At last we also need to install all build-dependencies."
echo "These actions need advanced privileges and may damage your system, it is recommended to use a VM."
echo "Do you want to continue? (y/n)"
read -r user_decission
echo " "
if [ "$user_decission" = "y" ]; then

  # Clean up existig files
  echo "Cleaning up environment."
  rm -rf "$DIR"/sogo*

  # Install debian packaging tools and download source
  echo "Installing dabian packaging tools."
  sudo apt-get install packaging-dev debian-keyring devscripts equivs -y > /dev/null 2>&1
  sudo touch /etc/apt/sources.list.d/testing_src.list > /dev/null 2>&1
  sudo echo "# Debian testing packages sources" > /etc/apt/sources.list.d/testing_src.list
  sudo echo "deb-src http://deb.debian.org/debian/ testing main" >> /etc/apt/sources.list.d/testing_src.list
  sudo apt update > /dev/null 2>&1
  echo "Downloading source files."
  apt source sogo/testing > /dev/null 2>&1

  # Modify source-files as necessary
  cd "$DIR"/sogo*/
  dch --bpo
  sed -i "s/Build-Depends: debhelper-compat (= 13),/Build-Depends: debhelper-compat (= 12),/g" "./debian/control"

  # Install build dependencies
  echo "Installing or building dependencies."
  sudo mk-build-deps --install --remove --tool='apt-get -o Debug::pkgProblemResolver=yes --no-install-recommends --yes'  > /dev/null 2>&1

  # Build the new package
  echo "Building the new packages."
  #fakeroot debian/rules binary
  dpkg-buildpackage -us -uc  > /dev/null 2>&1
  
  # Success message
  echo " "
  if [ $? -eq 0 ]; then
    echo "The following packages were produced."
    ls "$DIR"/ | grep .deb$
  else
    echo "No packages were produced."
  fi
  echo " "

else
  echo "Aborting..."
  echo "No actions were performed."
fi
