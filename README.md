# SOGo-Debian

This script will help you to produce a backport of SOGo-Groupware to Debian Buster.

The intention of this script is simply to backport all SOGo related packages from the official Debian Bullseye repository [Link](https://packages.debian.org/bullseye/sogo) to Debian Buster.\
This is achived using the knowledge of the article "SimpleBackportCreation" residing in the Debian wiki [Link](https://wiki.debian.org/SimpleBackportCreation).


The process is fully automated except the creation of a new changelog entry.\
You will be asked for your favorite text editor and after choosing one you will see the changelog with a new entry at the beginning.\
Now you can modify this entry as you please, or just let it be.\
But it is important to save that file before closing it.
